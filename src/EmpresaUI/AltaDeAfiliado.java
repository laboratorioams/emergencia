/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package EmpresaUI;



import Empresa.Empresa;
import excepciones.RepetidoException;
import gestionafiliados.Afiliado;
import gestionafiliados.Familiar;
import gestionafiliados.RepositorioDeAfiliados;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import javax.swing.JOptionPane;

/**
 *
 * @author Marcos
 */
public class AltaDeAfiliado extends javax.swing.JFrame {
    private final RepositorioDeAfiliados repositorioDeAfiliados = Empresa.instancia().getRepositorioDeAfiliados();
    private DateTimeFormatter formato = new DateTimeFormatterBuilder().append(DateTimeFormatter.ofPattern("d/M/yyyy")).toFormatter();

    /**
     * Creates new form AltaDeAfiliado
     */
    public AltaDeAfiliado() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelAltaDeAfiliado = new javax.swing.JPanel();
        txtNombre = new javax.swing.JLabel();
        txtApellido = new javax.swing.JLabel();
        txtFechaDeNacimiento = new javax.swing.JLabel();
        txtDni = new javax.swing.JLabel();
        txtFechaDeAfiliacion = new javax.swing.JLabel();
        nombreIngresado = new javax.swing.JTextField();
        apellidoIngresado = new javax.swing.JTextField();
        nacimientoIngresado = new javax.swing.JTextField();
        dniIngresado = new javax.swing.JTextField();
        fechaDeAfiliacionIngresada = new javax.swing.JTextField();
        botonAceptar = new javax.swing.JButton();
        formatoDeFecha = new javax.swing.JLabel();
        formatoDeFecha1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alta de Afiliado");
        setBounds(new java.awt.Rectangle(550, 200, 400, 300));
        setName("frameAltaDeAfiliado"); // NOI18N

        panelAltaDeAfiliado.setPreferredSize(new java.awt.Dimension(400, 300));

        txtNombre.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtNombre.setText("Nombre:");

        txtApellido.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtApellido.setText("Apellido:");

        txtFechaDeNacimiento.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtFechaDeNacimiento.setText("Fecha de nacimiento:");

        txtDni.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtDni.setText("Documento:");

        txtFechaDeAfiliacion.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtFechaDeAfiliacion.setText("Fecha de Afiliación:");

        nombreIngresado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreIngresadoActionPerformed(evt);
            }
        });

        nacimientoIngresado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nacimientoIngresadoActionPerformed(evt);
            }
        });

        botonAceptar.setFont(new java.awt.Font("Segoe UI", 1, 11)); // NOI18N
        botonAceptar.setText("ACEPTAR");
        botonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarActionPerformed(evt);
            }
        });

        formatoDeFecha.setText("   dia/mes/año");

        formatoDeFecha1.setText("   dia/mes/año");

        javax.swing.GroupLayout panelAltaDeAfiliadoLayout = new javax.swing.GroupLayout(panelAltaDeAfiliado);
        panelAltaDeAfiliado.setLayout(panelAltaDeAfiliadoLayout);
        panelAltaDeAfiliadoLayout.setHorizontalGroup(
            panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                        .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                                .addComponent(txtFechaDeAfiliacion)
                                .addGap(18, 18, 18)
                                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                                        .addComponent(formatoDeFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                                        .addComponent(fechaDeAfiliacionIngresada, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(botonAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(nombreIngresado, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                    .addComponent(apellidoIngresado)))
                            .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                                .addComponent(txtDni)
                                .addGap(18, 18, 18)
                                .addComponent(dniIngresado, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                        .addComponent(txtFechaDeNacimiento)
                        .addGap(18, 18, 18)
                        .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nacimientoIngresado, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(formatoDeFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        panelAltaDeAfiliadoLayout.setVerticalGroup(
            panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nombreIngresado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apellidoIngresado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtFechaDeNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nacimientoIngresado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(formatoDeFecha1)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dniIngresado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(panelAltaDeAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtFechaDeAfiliacion, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fechaDeAfiliacionIngresada, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelAltaDeAfiliadoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(formatoDeFecha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonAceptar)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAltaDeAfiliado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAltaDeAfiliado, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nombreIngresadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreIngresadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreIngresadoActionPerformed

    private void botonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarActionPerformed
        boolean encontrado = false;
        LocalDate fechaDeNacimiento = LocalDate.parse(nacimientoIngresado.getText(), formato);
        LocalDate fechaDeAfiliacion = LocalDate.parse(fechaDeAfiliacionIngresada.getText(), formato);
        if(sonLetras(nombreIngresado.getText()) && sonLetras(apellidoIngresado.getText())
        && sonNumeros(dniIngresado.getText())){
            Afiliado nuevoAfiliado = new  Afiliado(nombreIngresado.getText(),apellidoIngresado.getText(),fechaDeNacimiento,dniIngresado.getText(),fechaDeAfiliacion);
            try {
                for(Afiliado afiliado : repositorioDeAfiliados.obtenerAfiliados()){
                    if(afiliado.getDni().equals(dniIngresado.getText())){
                        encontrado = true;
                    }
                    for(Familiar familiar : afiliado.obtenerFamiliares()){
                        if(familiar.getDni().equals(dniIngresado.getText())){
                            encontrado = true;
                        }
                    }
                }
                if(encontrado == false){
                    repositorioDeAfiliados.agregarAfiliado(nuevoAfiliado);
                    JOptionPane.showMessageDialog(this,"Afiliado " + nuevoAfiliado.getApellido() + " DNI: " + nuevoAfiliado.getDni(),"Completado",JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    throw new RepetidoException("Persona ya existente");
                }
            } catch (RepetidoException ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
            }
            nombreIngresado.setText("");
            apellidoIngresado.setText("");
            nacimientoIngresado.setText("");
            dniIngresado.setText("");
            fechaDeAfiliacionIngresada.setText("");
        }
        else{
            JOptionPane.showMessageDialog(this,"Ingrese datos validos","Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_botonAceptarActionPerformed

    private void nacimientoIngresadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nacimientoIngresadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nacimientoIngresadoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AltaDeAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AltaDeAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AltaDeAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AltaDeAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AltaDeAfiliado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField apellidoIngresado;
    private javax.swing.JButton botonAceptar;
    private javax.swing.JTextField dniIngresado;
    private javax.swing.JTextField fechaDeAfiliacionIngresada;
    private javax.swing.JLabel formatoDeFecha;
    private javax.swing.JLabel formatoDeFecha1;
    private javax.swing.JTextField nacimientoIngresado;
    private javax.swing.JTextField nombreIngresado;
    private javax.swing.JPanel panelAltaDeAfiliado;
    private javax.swing.JLabel txtApellido;
    private javax.swing.JLabel txtDni;
    private javax.swing.JLabel txtFechaDeAfiliacion;
    private javax.swing.JLabel txtFechaDeNacimiento;
    private javax.swing.JLabel txtNombre;
    // End of variables declaration//GEN-END:variables
    public boolean sonLetras(String str) {
        return str.matches("[a-zA-Z]+");
    }
    
    public boolean sonNumeros(String str) {
        return str.matches("[0-9]+");
    }
}
