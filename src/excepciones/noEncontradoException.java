/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package excepciones;

/**
 *
 * @author Marcos
 */
public class NoEncontradoException extends Exception {

    public NoEncontradoException(String mensaje) {
        super(mensaje);
    }
    
}
