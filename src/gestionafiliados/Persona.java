/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliados;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Marcos
 */
public class Persona {
    
    private String nombre;
    private String apellido;
    private LocalDate fechaDeNacimiento;
    private String dni;

    public Persona(String nombre, String apellido, LocalDate fechaDeNacimiento, String dni) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.dni = dni;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public LocalDate getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public int getEdad() {
        
        int edad;
        edad = (LocalDate.now()).getYear() - fechaDeNacimiento.getYear();
        return edad;
    }
    
    @Override
    public boolean equals (Object objeto){
        
        if (objeto instanceof Persona){
                        
            if (this.dni.equals(((Persona) objeto).dni))
            {
                return true;
            }       
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.dni);
        return hash;
    }
}