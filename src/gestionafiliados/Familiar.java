/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliados;

import java.time.LocalDate;

/**
 *
 * @author Marcos
 */
public class Familiar extends Persona{
    
    private String rol;

    public Familiar(String rol, String nombre, String apellido, LocalDate fechaDeNacimiento, String dni) {
        super(nombre, apellido, fechaDeNacimiento, dni);
        this.rol = rol;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
    @Override
    public boolean equals (Object objeto){
        
        return (super.equals(objeto));
    }
}
