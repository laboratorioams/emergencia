/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliados;

import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class Afiliado extends Persona{
    
    private LocalDate fechaDeAfiliacion;
    private List<Familiar> familiares;
    //private List<Factura> facturasAsociadas;

    public Afiliado(String nombre, String apellido, LocalDate fechaDeNacimiento, String dni, LocalDate fechaDeAfiliacion) {
        super(nombre, apellido, fechaDeNacimiento, dni);
        familiares = new ArrayList<Familiar>();
        this.fechaDeAfiliacion = fechaDeAfiliacion;
    }

    public LocalDate getFechaDeAfiliacion() {
        return fechaDeAfiliacion;
    }

    public void setFechaDeAfiliacion(LocalDate fechaDeAfiliacion) {
        this.fechaDeAfiliacion = fechaDeAfiliacion;
    }
    
    @Override
    public boolean equals (Object objeto){
        
        return (super.equals(objeto));
    }

    public List<Familiar> obtenerFamiliares() {
        return familiares;
    }

    public void agregarFamiliar(Familiar nuevoFamiliar) throws RepetidoException{
        for(Familiar familiar : familiares){
            if(familiar.getDni().equals(nuevoFamiliar.getDni())){
                throw new RepetidoException("El familiar ya existe");
            }
        }
        familiares.add(nuevoFamiliar);
    }
    
    public Familiar buscarFamiliar(String dni) throws NoEncontradoException{
        
        for(Familiar familiar : familiares){
            if(familiar.getDni().equals(dni)){
                return familiar;
            }
        }
        throw new NoEncontradoException("Familiar no encontrado:");
    }
    
    public void eliminarFamiliar(Familiar familiar) throws NoEncontradoException{
        
        familiares.remove(buscarFamiliar(familiar.getDni()));
    }
}
