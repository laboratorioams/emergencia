/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliados;

import java.util.Objects;

/**
 *
 * @author Marcos
 */
public class Domicilio {
    
    private String calle;
    private String barrio;
    private String numeracion;
    private Departamento departamento;

    public Domicilio(String calle, String barrio, String numeracion) {
        this.calle = calle;
        this.barrio = barrio;
        this.numeracion = numeracion;
    }
       
    public Domicilio(String calle, String barrio, String numeracion, Departamento departamento) {
        this.calle = calle;
        this.barrio = barrio;
        this.numeracion = numeracion;
        this.departamento = departamento;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getNumeracion() {
        return numeracion;
    }

    public void setNumeracion(String numeracion) {
        this.numeracion = numeracion;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.calle);
        hash = 79 * hash + Objects.hashCode(this.barrio);
        hash = 79 * hash + Objects.hashCode(this.numeracion);
        hash = 79 * hash + Objects.hashCode(this.departamento);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        final Domicilio other = (Domicilio) obj;
        if(this.barrio.equals(other.barrio) && this.calle.equals(other.calle) 
            && this.numeracion.equals(other.numeracion)
            && this.departamento.piso.equals(other.departamento.piso)){
                return true;
        }
        return false;
    }
    
    public class Departamento {
    
        private String piso;

        public Departamento(String piso) {
            this.piso = piso;
        }

        public String getPiso() {
            return piso;
        }

        public void setPiso(String piso) {
            this.piso = piso;
        }
        
        @Override
        public boolean equals(Object obj){
            // Tu lógica de comparación personalizada aquí
            if (this == obj) {
                return true; // Si son la misma instancia, son iguales
            }

            if (obj == null || getClass() != obj.getClass()) {
                return false; // Si el objeto es nulo o no es de la misma clase, no son iguales
            }

            Departamento otra = (Departamento) obj; // Convertimos obj a una instancia de la clase
            return this.piso.equals(otra.piso); // Comparación basada en el contenido de los objetos
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + Objects.hashCode(this.piso);
            return hash;
        }
    }
}
