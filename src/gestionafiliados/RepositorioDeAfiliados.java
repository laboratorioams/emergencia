/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionafiliados;

import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class RepositorioDeAfiliados {
    
    private List<Afiliado> afiliados;

    public RepositorioDeAfiliados() {
        afiliados = new ArrayList<Afiliado>();
    }
    
    public List<Afiliado> obtenerAfiliados() {
        return afiliados;
    }

    public Afiliado buscarAfiliado(String dni) throws NoEncontradoException{
        
        for(Afiliado afiliado : afiliados){
            if(afiliado.getDni().equals(dni)){
                return afiliado;
            }
        }
        throw new NoEncontradoException("Afiliado no encontrado:");
    }

    public void agregarAfiliado(Afiliado nuevoAfiliado) throws RepetidoException {
        
        for(Afiliado afiliado : afiliados){
            if(afiliado.getDni().equals(nuevoAfiliado.getDni())){
                throw new RepetidoException("El afiliado ya existe");
            }
        }
        afiliados.add(nuevoAfiliado);
    }

    public void eliminarAfiliado(Afiliado afiliado) throws NoEncontradoException {
        
        afiliados.remove(buscarAfiliado(afiliado.getDni()));
    }
    
}
