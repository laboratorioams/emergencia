/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package gestionDePagos;

import gestionafiliados.Afiliado;
import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



/**
 *
 * @author exequ
 */



public class Factura {
    
    //ATRIBUTOS
    private int numeroFactura;
    private LocalDate fechaEmision;
    private Afiliado afiliado;
    private double tarifaPorFamiliar; 
    private List<Item> items;
    
    
    
    
    //CONSTRUCTORES
    public Factura(int numeroFactura, LocalDate fechaEmision) {
        this.numeroFactura = numeroFactura;
        this.fechaEmision = fechaEmision;
        items = new ArrayList<Item>();
    } 
    
    //SETTERS AND GETTERS
    public int getNumeroFactura() {    
        return numeroFactura; 
    }

    public void setNumeroFactura(int numeroFactura) {
        this.numeroFactura = numeroFactura;
    }
    
    public LocalDate getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(LocalDate fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public double getTarifaPorFamiliar() {
        return tarifaPorFamiliar;
    }

    public void setTarifaPorFamiliar(double tarifaPorFamiliar) {
        this.tarifaPorFamiliar = tarifaPorFamiliar;
    }
    
    public List<Item> obtenerItems() {
        return items;
    }
    
    public void agregarItem(Item nuevoItem) throws RepetidoException{
        for(Item item : items){
            if(item.getDescripcion().equals(nuevoItem.getDescripcion())){
                throw new RepetidoException("El item ya existe");
            }
        }
        items.add(nuevoItem);
    }
    
    public Item buscarItem(String concepto) throws NoEncontradoException{
        
        for(Item item : items){
            if(item.getDescripcion().equals(concepto)){
                return item;
            }
        }
        throw new NoEncontradoException("Item no encontrado:");
    }
    
    public void eliminarItem(Item item) throws NoEncontradoException{
        
        items.remove(buscarItem(item.getDescripcion()));
    }
    
    public double montoTotal(){
        double montoTotal = 0;
        int i;
        for(Item item : items){
            montoTotal = montoTotal + item.totalItem();
        }
        for(i=0;i<(afiliado.obtenerFamiliares()).size();i++){
            montoTotal = montoTotal + tarifaPorFamiliar;
        }
        return montoTotal;
    }
    
    @Override
    public boolean equals(Object obj) {
        // Tu lógica de comparación personalizada aquí
        if (this == obj) {
            return true; // Si son la misma instancia, son iguales
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false; // Si el objeto es nulo o no es de la misma clase, no son iguales
        }

        Factura otra = (Factura) obj; // Convertimos obj a una instancia de la clase
        return numeroFactura == otra.numeroFactura; // Comparación basada en el contenido de los objetos
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.numeroFactura;
        return hash;
    }
    
    public class Item {
    
        private float monto;
        private int cantidad;
        private int iva;
        private String descripcion;

        public Item(float monto, int cantidad, int iva, String concepto) {
            this.monto = monto;
            this.cantidad = cantidad;
            this.iva = iva;
            this.descripcion = concepto;
        }

        public float getMonto() {
            return monto;
        }

        public int getCantidad() {
            return cantidad;
        }

        public float getIva() {
            return iva;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setMonto(float monto) {
            this.monto = monto;
        }

        public void setCantidad(int cantidad) {
            this.cantidad = cantidad;
        }

        public void setIva(int iva) {
            this.iva = iva;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public double totalItem(){
            return ((monto*cantidad) - (((monto*cantidad)*iva)/100));
        }

        @Override
        public boolean equals(Object obj){
            // Tu lógica de comparación personalizada aquí
            if (this == obj) {
                return true; // Si son la misma instancia, son iguales
            }

            if (obj == null || getClass() != obj.getClass()) {
                return false; // Si el objeto es nulo o no es de la misma clase, no son iguales
            }

            Item otra = (Item) obj; // Convertimos obj a una instancia de la clase
            return this.descripcion.equals(otra.descripcion); // Comparación basada en el contenido de los objetos
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 11 * hash + Objects.hashCode(this.descripcion);
            return hash;
        }
    }
}
