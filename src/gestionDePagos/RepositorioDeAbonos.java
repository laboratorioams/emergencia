/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionDePagos;

import java.util.ArrayList;
import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.time.LocalDate;
import java.util.List;



/**
 *
 * @author exequ
 */


public class RepositorioDeAbonos {
    
    private List<Factura> facturas;
    
    public RepositorioDeAbonos() {
        facturas = new ArrayList<Factura>();
    }
    
    public List<Factura> obtenerFacturas() {
        return facturas;
    }

    public void agregarFactura(Factura nuevaFactura) throws RepetidoException{
        for(Factura factura : facturas){
            if(factura.getNumeroFactura()==nuevaFactura.getNumeroFactura()){
                throw new RepetidoException("La factura ya existe");
            }
        }
        facturas.add(nuevaFactura);
    }
    
    public Factura buscarFactura(int numeroFactura) throws NoEncontradoException{
        
        for(Factura factura : facturas){
            if(factura.getNumeroFactura()==numeroFactura){
                return factura;
            }
        }
        throw new NoEncontradoException("Factura no encontrada:");
    }
    
    public Factura buscarFactura(LocalDate fecha, int numeroFactura) throws NoEncontradoException{
        
        for(Factura factura : facturas){
            if(factura.getFechaEmision().equals(fecha) && factura.getNumeroFactura()==numeroFactura){
                return factura;
            }
        }
        throw new NoEncontradoException("Factura no encontrada:");
    }
    
    public void eliminarFactura(Factura factura) throws NoEncontradoException{
        
        facturas.remove(buscarFactura(factura.getNumeroFactura()));
    }
}
