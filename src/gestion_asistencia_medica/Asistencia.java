/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestion_asistencia_medica;

import gestion_moviles.Movil;
import gestionafiliados.Domicilio;
import gestionafiliados.Persona;
import gestionempleados.Chofer;
import gestionempleados.Doctor;
import gestionempleados.Enfermero;
import java.time.LocalDate;

/**
 *
 * @author Marcos
 */
public class Asistencia extends Solicitud{
    
    private String tipoDeAtencion;
    private String diagnostico;

    public Asistencia(LocalDate fechaSolicitud, Persona solicitante, Domicilio domicilio, Doctor doctorAsigando, Enfermero enfermeroAsignado, Chofer choferAsignado, Movil movilAsignado, String nroOperacion, String tipoDeAtencion, String diagnostico) {
        super(fechaSolicitud, solicitante, domicilio, doctorAsigando, enfermeroAsignado, choferAsignado, movilAsignado, nroOperacion);
        this.tipoDeAtencion = tipoDeAtencion;
        this.diagnostico = diagnostico;
    }

    public String getTipoDeAtencion() {
        return tipoDeAtencion;
    }

    public void setTipoDeAtencion(String tipoDeAtencion) {
        this.tipoDeAtencion = tipoDeAtencion;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }
    
    @Override
    public boolean equals(Object obj){
        return (super.equals(obj));
    }
}
