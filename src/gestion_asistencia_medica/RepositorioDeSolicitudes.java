/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestion_asistencia_medica;

import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class RepositorioDeSolicitudes {
    
    private List<Solicitud> solicitudes;

    public RepositorioDeSolicitudes() {
        solicitudes = new ArrayList<Solicitud>();
    }
    
    public List<Solicitud> obtenerSolicitudes() {
        return solicitudes;
    }

    public void agregarSolicitud(Solicitud nuevaSolicitud) throws RepetidoException{
        //condicional. el afiliado se encuentra en 2 meses de mora?
        //condicional. si no es afiliado, es familiar de un afiliado y en condiciones?
        //condicional. hay choferes disponibles?
        //condicional. hay moviles disponibles?
        for(Solicitud solicitud : solicitudes){
            if(solicitud.getNroOperacion().equals(nuevaSolicitud.getNroOperacion())){
                throw new RepetidoException("La solicitud ya existe");
            }
        }
        solicitudes.add(nuevaSolicitud);
    }
    
    public Solicitud buscarSolicitud(String nroOperacion) throws NoEncontradoException{
        
        for(Solicitud solicitud : solicitudes){
            if(solicitud.getNroOperacion().equals(nroOperacion)){
                return solicitud;
            }
        }
        throw new NoEncontradoException("Solicitud no encontrada:");
    }
    
    public Solicitud buscarSolicitud(LocalDate fecha, String nroOperacion) throws NoEncontradoException{
        
        for(Solicitud solicitud : solicitudes){
            if(solicitud.getFechaSolicitud().equals(fecha) && solicitud.getNroOperacion().equals(nroOperacion)){
                return solicitud;
            }
        }
        throw new NoEncontradoException("Solicitud no encontrada:");
    }
    
    public void eliminarSolicitud(Solicitud solicitud) throws NoEncontradoException{
        
        solicitudes.remove(buscarSolicitud(solicitud.getNroOperacion()));
    }
}
