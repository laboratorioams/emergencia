/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestion_asistencia_medica;

import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class RepositorioDeAsistencias {
    
    private List<Asistencia> asistencias;

    public RepositorioDeAsistencias() {
        asistencias = new ArrayList<Asistencia>();
    }
    
    public List<Asistencia> obtenerAsistencias() {
        return asistencias;
    }

    public void agregarAsistencia(Asistencia nuevaAsistencia) throws RepetidoException{
        for(Asistencia asistencia : asistencias){
            if(asistencia.getNroOperacion().equals(nuevaAsistencia.getNroOperacion())){
                throw new RepetidoException("La asistencia ya existe");
            }
        }
        asistencias.add(nuevaAsistencia);
    }
    
    public Asistencia buscarAsistencia(String nroOperacion) throws NoEncontradoException{
        
        for(Asistencia asistencia : asistencias){
            if(asistencia.getNroOperacion().equals(nroOperacion)){
                return asistencia;
            }
        }
        throw new NoEncontradoException("Asistencia no encontrada:");
    }
    
    public Asistencia buscarAsistencia(LocalDate fecha, String nroOperacion) throws NoEncontradoException{
        
        for(Asistencia asistencia : asistencias){
            if(asistencia.getFechaSolicitud().equals(fecha) && asistencia.getNroOperacion().equals(nroOperacion)){
                return asistencia;
            }
        }
        throw new NoEncontradoException("Asistencia no encontrada:");
    }
    
    public void eliminarAsistencia(Asistencia asistencia) throws NoEncontradoException{
        
        asistencias.remove(buscarAsistencia(asistencia.getNroOperacion()));
    }
}
