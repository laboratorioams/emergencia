package gestion_asistencia_medica;
import gestion_moviles.Movil;
import gestionafiliados.Domicilio;
import java.time.LocalDate;
import gestionafiliados.Persona;
import gestionempleados.Chofer;
import gestionempleados.Doctor;
import gestionempleados.Enfermero;
import java.util.Objects;

public class Solicitud {
    private LocalDate fechaSolicitud;
    private Persona solicitante;
    private Doctor doctorAsigando;
    private Enfermero enfermeroAsignado;
    private Chofer choferAsignado;
    private Movil movilAsignado;
    private Domicilio domicilio;
    private String nroOperacion;
    
    //Constructores

    public Solicitud(LocalDate fechaSolicitud, Persona solicitante, Domicilio domicilio, Doctor doctorAsigando, Enfermero enfermeroAsignado, Chofer choferAsignado, Movil movilAsignado, String nroOperacion) {
        this.fechaSolicitud = fechaSolicitud;
        this.solicitante = solicitante;
        this.domicilio = domicilio;
        this.doctorAsigando = doctorAsigando;
        this.enfermeroAsignado = enfermeroAsignado;
        this.choferAsignado = choferAsignado;
        this.movilAsignado = movilAsignado;
        this.nroOperacion = nroOperacion;
    }

//Getters
    
    public LocalDate getFechaSolicitud() {
        return fechaSolicitud;
    }

    public String getNroOperacion() {
        return nroOperacion;
    }

    public Persona getSolicitante() {
        return solicitante;
    }

    public Doctor getDoctorAsigando() {
        return doctorAsigando;
    }

    public Enfermero getEnfermeroAsignado() {
        return enfermeroAsignado;
    }

    public Chofer getChoferAsignado() {
        return choferAsignado;
    }

    public Movil getMovilAsignado() {
        return movilAsignado;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setFechaSolicitud(LocalDate fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public void setSolicitante(Persona solicitante) {
        this.solicitante = solicitante;
    }

    public void setDoctorAsigando(Doctor doctorAsigando) {
        this.doctorAsigando = doctorAsigando;
    }

    public void setEnfermeroAsignado(Enfermero enfermeroAsignado) {
        this.enfermeroAsignado = enfermeroAsignado;
    }

    public void setChoferAsignado(Chofer choferAsignado) {
        this.choferAsignado = choferAsignado;
    }

    public void setMovilAsignado(Movil movilAsignado) {
        this.movilAsignado = movilAsignado;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public void setNroOperacion(String nroOperacion) {
        this.nroOperacion = nroOperacion;
    }
    
    
   @Override
    public boolean equals (Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        final Solicitud other = (Solicitud) obj;
        return this.nroOperacion.equals(other.nroOperacion);   
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nroOperacion);
        return hash;
    }
}
