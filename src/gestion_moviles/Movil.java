package gestion_moviles;

import java.util.Objects;


public class Movil {
    private String marca;
    private String modelo;
    private String anio;
    private String patente;
    
//Constructor
    public Movil(String Marca, String Modelo, String anio, String Patente) {
        this.marca = Marca;
        this.modelo = Modelo;
        this.anio = anio;
        this.patente = Patente;
    }

//Getters
    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getAnio() {
        return anio;
    }

    public String getPatente() {
        return patente;
    }

//Setters

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }
    
    @Override
    public boolean equals (Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        final Movil other = (Movil) obj;
        return this.patente.equals(other.patente);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.patente);
        return hash;
    }
}
