/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestion_moviles;

import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class RepositorioDeMoviles {
    
    private List<Movil> moviles;

    public RepositorioDeMoviles() {
        moviles = new ArrayList<Movil>();
    }
    
    public List<Movil> obtenerMoviles() {
        return moviles;
    }

    public void agregarMovil(Movil nuevoMovil) throws RepetidoException{
        for(Movil movil : moviles){
            if(movil.getPatente().equals(nuevoMovil.getPatente())){
                throw new RepetidoException("El movil ya existe");
            }
        }
        moviles.add(nuevoMovil);
    }
    
    public Movil buscarMovil(String patente) throws NoEncontradoException{
        
        for(Movil movil : moviles){
            if(movil.getPatente().equals(patente)){
                return movil;
            }
        }
        throw new NoEncontradoException("Movil no encontrado:");
    }
    
    public void eliminarMovil(Movil movil) throws NoEncontradoException{
        
        moviles.remove(buscarMovil(movil.getPatente()));
    }
}
