/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import java.time.LocalDate;

/**
 *
 * @author Marcos
 */
public class Administrativo extends Empleado{

    public Administrativo(String nombre, String apellido, LocalDate fechaDeNacimiento, String dni, double sueldo) {
        super(nombre, apellido, fechaDeNacimiento, dni, sueldo);
    }
    
    @Override
    public boolean equals (Object objeto){
        
        return (super.equals(objeto));
    }
}
