/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import gestionafiliados.Persona;
import java.time.LocalDate;

/**
 *
 * @author Marcos
 */
public class Empleado extends Persona{
    
    private double sueldo;

    public Empleado(String nombre, String apellido, LocalDate fechaDeNacimiento, String dni, double sueldo) {
        super(nombre, apellido, fechaDeNacimiento, dni);
        this.sueldo = sueldo;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }
    
    @Override
    public boolean equals (Object objeto){
        
        return (super.equals(objeto));
    }
}
