/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import excepciones.NoEncontradoException;
import excepciones.RepetidoException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
public class RepositorioDeEmpleados {
    
    private List<Empleado> empleados;

    public RepositorioDeEmpleados() {
        empleados = new ArrayList<Empleado>();
    }
    
    public List<Empleado> obtenerEmpleados() {
        return empleados;
    }

    public Empleado buscarEmpleado(String dni) throws NoEncontradoException{
        
        for(Empleado empleado : empleados){
            if(empleado.getDni().equals(dni)){
                return empleado;
            }
        }
        throw new NoEncontradoException("Empleado no encontrado:");
    }

    public void agregarEmpleado(Empleado nuevoEmpleado) throws RepetidoException {
        
        for(Empleado empleado : empleados){
            if(empleado.getDni().equals(nuevoEmpleado.getDni())){
                throw new RepetidoException("El empleado ya existe");
            }
        }
        empleados.add(nuevoEmpleado);
    }

    public void eliminarEmpleado(Empleado empleado) throws NoEncontradoException {
        
        empleados.remove(buscarEmpleado(empleado.getDni()));
    } 
}
