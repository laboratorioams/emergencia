/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionempleados;

import java.time.LocalDate;

/**
 *
 * @author Marcos
 */
public class Chofer extends Empleado{
    
    private String numeroDeLicencia;

    public Chofer(String nombre, String apellido, LocalDate fechaDeNacimiento, String dni, double sueldo, String numeroDeLicencia) {
        super(nombre, apellido, fechaDeNacimiento, dni, sueldo);
        this.numeroDeLicencia = numeroDeLicencia;
    }
    
    @Override
    public boolean equals (Object objeto){
        
        return (super.equals(objeto));
    }
}
