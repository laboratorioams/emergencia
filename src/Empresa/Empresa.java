package Empresa;




import gestionDePagos.RepositorioDeAbonos;
import gestion_asistencia_medica.RepositorioDeAsistencias;
import gestion_asistencia_medica.RepositorioDeSolicitudes;
import gestion_moviles.RepositorioDeMoviles;
import gestionafiliados.RepositorioDeAfiliados;
import gestionempleados.RepositorioDeEmpleados;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Marcos
 */
public class Empresa {
    
    private static Empresa empresa;
    
    private RepositorioDeAfiliados repositorioDeAfiliados;
    private RepositorioDeEmpleados repositorioDeEmpleados;
    private RepositorioDeSolicitudes repositorioDeSolicitudes;
    private RepositorioDeAsistencias repositorioDeAsistencias;
    private RepositorioDeMoviles repositorioDeMoviles;
    private RepositorioDeAbonos repositorioDeAbonos;
    
    private Empresa(){
        repositorioDeAfiliados = new RepositorioDeAfiliados();
        repositorioDeEmpleados = new RepositorioDeEmpleados();
        repositorioDeSolicitudes = new RepositorioDeSolicitudes();
        repositorioDeAsistencias = new RepositorioDeAsistencias();
        repositorioDeMoviles = new RepositorioDeMoviles();
        repositorioDeAbonos = new RepositorioDeAbonos();
    }
    
    public static Empresa instancia(){
        if(empresa == null){
            empresa = new Empresa();
        }
        return empresa;
    }

    public RepositorioDeAfiliados getRepositorioDeAfiliados() {
        return repositorioDeAfiliados;
    }

    public RepositorioDeEmpleados getRepositorioDeEmpleados() {
        return repositorioDeEmpleados;
    }

    public RepositorioDeSolicitudes getRepositorioDeSolicitudes() {
        return repositorioDeSolicitudes;
    }

    public RepositorioDeAsistencias getRepositorioDeAsistencias() {
        return repositorioDeAsistencias;
    }

    public RepositorioDeMoviles getRepositorioDeMoviles() {
        return repositorioDeMoviles;
    }

    public RepositorioDeAbonos getRepositorioDeAbonos() {
        return repositorioDeAbonos;
    }
}
